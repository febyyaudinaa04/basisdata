<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75',
            'content' => 'Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.',
            'category' => 'HUT RI Ke-75'
        ]);
    }
}
